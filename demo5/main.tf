
# ------ Retrieve Regional / Cloud Data
# -------- Get a list of Availability Domains
data "oci_identity_availability_domains" "AvailabilityDomains" {
    compartment_id = var.compartment_ocid
}
data "template_file" "AvailabilityDomainNames" {
    count    = length(data.oci_identity_availability_domains.AvailabilityDomains.availability_domains)
    template = data.oci_identity_availability_domains.AvailabilityDomains.availability_domains[count.index]["name"]
}
# -------- Get a list of Fault Domains
data "oci_identity_fault_domains" "FaultDomainsAD1" {
    availability_domain = element(data.oci_identity_availability_domains.AvailabilityDomains.availability_domains, 0)["name"]
    compartment_id = var.compartment_ocid
}
data "oci_identity_fault_domains" "FaultDomainsAD2" {
    availability_domain = element(data.oci_identity_availability_domains.AvailabilityDomains.availability_domains, 1)["name"]
    compartment_id = var.compartment_ocid
}
data "oci_identity_fault_domains" "FaultDomainsAD3" {
    availability_domain = element(data.oci_identity_availability_domains.AvailabilityDomains.availability_domains, 2)["name"]
    compartment_id = var.compartment_ocid
}
# -------- Get Home Region Name
data "oci_identity_region_subscriptions" "RegionSubscriptions" {
    tenancy_id = var.tenancy_ocid
}
data "oci_identity_regions" "Regions" {
}
data "oci_identity_tenancy" "Tenancy" {
    tenancy_id = var.tenancy_ocid
}

locals {
#    HomeRegion = [for x in data.oci_identity_region_subscriptions.RegionSubscriptions.region_subscriptions: x if x.is_home_region][0]
    home_region = lookup(
        {
            for r in data.oci_identity_regions.Regions.regions : r.key => r.name
        },
        data.oci_identity_tenancy.Tenancy.home_region_key
    )
}
# ------ Get List Service OCIDs
data "oci_core_services" "RegionServices" {
}
# ------ Get List Images
data "oci_core_images" "InstanceImages" {
    compartment_id           = var.compartment_ocid
}

# ------ Home Region Provider
provider "oci" {
    alias            = "home_region"
    region           = local.home_region
}

# ------ Create Compartment - Root True
# ------ Root Compartment
locals {
    Jahangir_id              = var.compartment_ocid
}

output "JahangirId" {
    value = local.Jahangir_id
}

# ------ Create Virtual Cloud Network
resource "oci_core_vcn" "Vcn1" {
    # Required
    compartment_id = local.Jahangir_id
    cidr_block     = "10.0.0.0/16"
    # Optional
    dns_label      = "vcn1"
    display_name   = "vcn1"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    Vcn1_id                       = oci_core_vcn.Vcn1.id
    Vcn1_dhcp_options_id          = oci_core_vcn.Vcn1.default_dhcp_options_id
    Vcn1_domain_name              = oci_core_vcn.Vcn1.vcn_domain_name
    Vcn1_default_dhcp_options_id  = oci_core_vcn.Vcn1.default_dhcp_options_id
    Vcn1_default_security_list_id = oci_core_vcn.Vcn1.default_security_list_id
    Vcn1_default_route_table_id   = oci_core_vcn.Vcn1.default_route_table_id
}


# ------ Create Internet Gateway
resource "oci_core_internet_gateway" "Igw1" {
    # Required
    compartment_id = local.Jahangir_id
    vcn_id         = local.Vcn1_id
    # Optional
    enabled        = true
    display_name   = "IGW1"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    Igw1_id = oci_core_internet_gateway.Igw1.id
}

# ------ Create Security List
# ------- Update VCN Default Security List
resource "oci_core_default_security_list" "Sl_Public" {
    # Required
    manage_default_resource_id = local.Vcn1_default_security_list_id
    egress_security_rules {
        # Required
        protocol    = "all"
        destination = "0.0.0.0/0"
        # Optional
        destination_type  = "CIDR_BLOCK"
    }
    ingress_security_rules {
        # Required
        protocol    = "6"
        source      = "0.0.0.0/0"
        # Optional
        source_type  = "CIDR_BLOCK"
        tcp_options {
            min = "22"
            max = "22"
        }
    }
    ingress_security_rules {
        # Required
        protocol    = "6"
        source      = "0.0.0.0/0"
        # Optional
        source_type  = "CIDR_BLOCK"
        tcp_options {
            min = "80"
            max = "80"
        }
    }
    # Optional
    display_name   = "SL_Public"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    Sl_Public_id = oci_core_default_security_list.Sl_Public.id
}


# ------ Create Security List
resource "oci_core_security_list" "DefaultSecurityListForVcn1" {
    # Required
    compartment_id = local.Jahangir_id
    vcn_id         = local.Vcn1_id
    egress_security_rules {
        # Required
        protocol    = "all"
        destination = "0.0.0.0/0"
        # Optional
        destination_type  = "CIDR_BLOCK"
    }
    ingress_security_rules {
        # Required
        protocol    = "6"
        source      = "0.0.0.0/0"
        # Optional
        source_type  = "CIDR_BLOCK"
        tcp_options {
            min = "22"
            max = "22"
        }
    }
    ingress_security_rules {
        # Required
        protocol    = "1"
        source      = "0.0.0.0/0"
        # Optional
        source_type  = "CIDR_BLOCK"
        icmp_options {
            type = "3"
            code = "4"
        }
    }
    ingress_security_rules {
        # Required
        protocol    = "1"
        source      = "10.0.0.0/16"
        # Optional
        source_type  = "CIDR_BLOCK"
        icmp_options {
            type = "3"
        }
    }
    # Optional
    display_name   = "Default Security List for vcn1"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    DefaultSecurityListForVcn1_id = oci_core_security_list.DefaultSecurityListForVcn1.id
}


# ------ Create Route Table
# ------- Update VCN Default Route Table
resource "oci_core_default_route_table" "Rt_Public" {
    # Required
    manage_default_resource_id = local.Vcn1_default_route_table_id
    route_rules    {
        destination_type  = "CIDR_BLOCK"
        destination       = "0.0.0.0/0"
        network_entity_id = local.Igw1_id
        description       = "Rule 01"
    }
    # Optional
    display_name   = "RT_Public"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    Rt_Public_id = oci_core_default_route_table.Rt_Public.id
    }


# ------ Create Route Table
resource "oci_core_route_table" "DefaultRouteTableForVcn1" {
    # Required
    compartment_id = local.Jahangir_id
    vcn_id         = local.Vcn1_id
    # Optional
    display_name   = "Default Route Table for vcn1"
    defined_tags   = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    DefaultRouteTableForVcn1_id = oci_core_route_table.DefaultRouteTableForVcn1.id
}


# ------ Create Subnet
# ---- Create Public Subnet
resource "oci_core_subnet" "Subnet_Public" {
    # Required
    compartment_id             = local.Jahangir_id
    vcn_id                     = local.Vcn1_id
    cidr_block                 = "10.0.0.0/24"
    # Optional
    display_name               = "Subnet_Public"
    dns_label                  = "subnetpublic"
    security_list_ids          = [local.Sl_Public_id]
    route_table_id             = local.Rt_Public_id
    dhcp_options_id            = local.Vcn1_dhcp_options_id
    prohibit_public_ip_on_vnic = false
    defined_tags               = {"Owner.createtBy": "oracleidentitycloudservice/jahangir.alam@oracle.com"}
}

locals {
    Subnet_Public_id              = oci_core_subnet.Subnet_Public.id
    Subnet_Public_domain_name     = oci_core_subnet.Subnet_Public.subnet_domain_name
}
